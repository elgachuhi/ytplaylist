'use strict';

module.exports = function (grunt) {

  require('jit-grunt')(grunt, {});

  var appConfig = {
    app: require('./bower.json').appPath || 'static'
  };

  grunt.initConfig({
    appConfig: appConfig,
    watch: {
        styles: {
        files: ['./static/styles/scss/*.scss'],
        tasks: ['newer:sass']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      }
    },
    //
    // Compile scss to css
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: './static/styles/scss',
          src: ['*.scss'],
          dest: './static/styles',
          ext: '.css'
        }]
      }
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
       src: [
        'Gruntfile.js',
          './static/scripts/{,*/}*.js'
       ]
      },
      test: {
           options: {
              jshintrc: './static/test/.jshintrc'
            },
            src: ['./static/scripts/test/spec/{,}*.js']
        }
    },
  });

  grunt.registerTask('default', [
    'newer:sass',
    'jshint:all',
    'watch'
  ]);
};
