'use strict';

/**
 * @ngdoc overview
 * @name ytVidsApp
 * @description
 * # ytVidsApp
 *
 * Main module of the application.
 */
var ytVidApp = angular.module('ytVidsApp', [ 'ngRoute']);

ytVidApp.config(['$routeProvider',
  function($routeProvider){
    $routeProvider.
      when('/videos', {
        templateUrl: 'static/views/main.html',
        controller: 'PlaylistCtrl'
      })
      .when('/videos/:videoId',{
        templateUrl:'static/views/detail.html',
        controller: 'VideoCtrl'
      })
      .otherwise({ redirectTo: '/videos' });
  }
]);
