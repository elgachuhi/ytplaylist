'use strict';

/**
 * @name ytVidsApp.controller:PlaylistCtrl
 * @description
 * # PlaylistCtrl
 * Controller of the ytVidsApp
 * #/videos or /
 */
angular.module('ytVidsApp')
  .controller('PlaylistCtrl', ['$scope','$timeout','YTService', function($scope,$timeout, YTService) {

    $scope.videos = [];
    $scope.loading = true;
    $scope.nextPageToken = '';

    $scope.loadVideos = function(){
      if( $scope.loadVideos.lastToken === $scope.nextPageToken ){ return; }
      YTService.getVideos($scope.nextPageToken, function(err, data){
        $scope.loading = true;
        if(data){
          var last = data.items.length - 1;
          for(var i = 0; i <= last; i++){
            $scope.videos.push(data.items[i]);
          }

          $scope.loading = false;
          $scope.nextPageToken = data.nextPageToken;
        }else{
          $scope.showError = true;
          $scope.errorMsg = 'There was problem getting vidoes from youtube, try again later';
          $scope.loading = false;

          $timeout(function(){
            $scope.showError = false;
          }, 2000);

          console.log('Error getting yt videos ==>' + err);
        }
      });

      // cache last token used to prevent unecessay api calls
      $scope.loadVideos.lastToken = $scope.nextPageToken;
    };

    $scope.loadVideos();
  }]);
