
'use strict';

/**
 * @name ytVidsApp.controller:VideoCtrl
 * @description
 * # VideoCtrl
 * Controller of the ytVidsApp
 * #/videos/:videoId
 */
angular.module('ytVidsApp')
  .controller('VideoCtrl', ['$timeout',
    '$scope', '$routeParams',
    'YTService',
    function($timeout, $scope, $routeParams, YTService) {
      $scope.videoId = $routeParams.videoId;
      $scope.playerLoadError = false;

      YTService.getVideo($scope.videoId, function(err, data){
        if(data){
          $scope.video = data;
        }else{
          $scope.errorMsg = 'There was a problem getting video data, try again later';
          $scope.showError = true;

          $timeout(function(){
            $scope.showError = false;
          }, 2000);

          console.log('Error getting yt video ==>' + err);
        }
      });
  }]);
