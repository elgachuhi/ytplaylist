/**
 * Directives module
 * directives of ytVidsApp
 */
'use strict';
angular.module('ytVidsApp')
  /*
   * Youtube player initializing directive
   */
  .directive('ytEmbed', ['$timeout',function($timeout){
    function link(scope){
      var tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        var playerVars = {
          'autoplay': 0,
          'rel': 1,
          'controls':1,
          'showinfo':1
        };

        $timeout(function(){
          try{
            new YT.Player('yt-iframe', {
              playerVars: playerVars ,
              height:'100%',
              width: '100%',
              videoId: scope.videoId,
              events:{
                'onReady': function(){
                  scope.playerLoaded = true;
                }
              }
            });
          }
          catch(e){
            console.log('playerError ' + e);
            scope.playerLoadError = true;
          }
        }, 1000);
    }
    return { link: link };
  }])
  /*
   * Simple Infinite scroll pagination
   */
  .directive('infiniteScroll', [function(){
    return {
      link: function(scope, ele){
        var raw = ele[0];
        ele.bind('scroll', function() {
          if (raw.scrollTop + raw.offsetHeight + 5 >= raw.scrollHeight) {
            console.log('here')
            if(scope.nextPageToken){ scope.loadVideos(); }
          }
        });
      }
    };
  }]);
