
'use strict';

angular.module('ytVidsApp')
  .service('YTService', ['$http', function($http) {
    function getPlaylist(nextPageToken, fn){
      $http({
        url: 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet,contentDetails,status&maxResults=20&playlistId=PLSi28iDfECJPJYFA4wjlF5KUucFvc0qbQ&key=AIzaSyCuv_16onZRx3qHDStC-FUp__A6si-fStw&pageToken='+nextPageToken,
        method: 'GET'
      })
      .success( function(data){
        fn(null, data);
      })
      .error(function(error){
        fn(error, null);
      });
    }

    function getVideo(videoId, fn){
      $http({
        url:'https://www.googleapis.com/youtube/v3/videos?part=snippet&key=AIzaSyCuv_16onZRx3qHDStC-FUp__A6si-fStw&id='+videoId,
        method: 'GET'
      })
      .success(function(data){
        fn(null, data.items[0].snippet);
      })
      .error(function(error){
        fn(error, null);
      });
    }

    return {
      getVideos: getPlaylist,
      getVideo: getVideo
    };
  }]);
