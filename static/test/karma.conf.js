module.exports = function(config) {
  'use strict';

  config.set({
    autoWatch: true,

    basePath: '../',

    frameworks: [
      'jasmine'
    ],

    files: [
      // bower:js
      'bower_components/angular/angular.js',
      'bower_components/angular-route/angular-route.js',
      'bower_components/angular-mocks/angular-mocks.js',
      // endbower

      'scripts/app.js',
      'scripts/controllers/playlist.js',
      'scripts/controllers/video.js',
      'scripts/services/ytService.js',
      'test/spec/**/*.js'
    ],

    exclude: [ ],
    port: 8080,

    browsers: [
      'Firefox'
    ],
    reporters: ['spec'],
    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-spec-reporter',
            'karma-jasmine'
            ],

    singleRun: true,
    colors: true,
    logLevel: config.LOG_INFO
  });
};
