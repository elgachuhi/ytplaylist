'use strict';

describe('Controller: PlayListCtrl', function () {

  beforeEach(module('ytVidsApp'));
  var PlaylistCtrl, scope, data;

  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PlaylistCtrl = $controller('PlaylistCtrl', {
      $scope: scope
    });
  }));

  beforeEach(inject(function(YTService){
    YTService.getVideos();
  }))

  it('intial videos should be 0', function () {
    expect(scope.videos.length).toBe(0);
  });

  it('Call to get videos should have 10 results',function(){
    //
    expect(scope.videos).not.toBe(null);
  });
});
