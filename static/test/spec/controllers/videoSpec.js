'use strict';

describe('Controller: videoCtrl', function () {

  beforeEach(module('ytVidsApp'));
  var VideoCtrl, scope;

  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VideoCtrl = $controller('VideoCtrl', {
      $scope: scope
    });

  }));

  it('playerLoaded loaded flag should be false', function(){
    expect(scope.playerLoadError).toBe(false);
  });

  it('should return no data if no video id is specified', inject(function(YTService){
    YTService.getVideo('', function(err, data){
      expect(data).toBe(null);
    })
  }));

  it('should return data if videoId is specified', inject(function(YTService){
    YTService.getVideo('X0qwQqwKLlM', function(err, data){
      expect(data).not.toBe(null);
    })
  }));
});
